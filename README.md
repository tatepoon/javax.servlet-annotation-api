# Project Description

Extract the annotation package from servlet 3.1 to be used in servlet 2.5.

# Source

```
#!xml

<dependency>
	<groupId>javax.servlet</groupId>
	<artifactId>javax.servlet-api</artifactId>
	<version>3.1.0</version>
</dependency>

```

# Modification

1. Only Java files under the annotation package are kept.
2. WebFilter annotation is removed due its dependence outside the annotation package.